# Projeto de Conclusão de Curso - Fatec Bragança Paulista

<h1> Catraca Digital </h1>

<b>Objetivo:</b> Aplicação desenvolvida para controlar o acesso dos alunos da Unidade.
O controle de acesso pode ser com a leitura de um codigo de barras e/ou QRCODE da carteirinha do Estudante. <br>

Ao realizar a leitura, é exibido em uma tela a foto do aluno e/ou visitante e alguns dados básicos para identificação.

Para os visitantes é possivel realizar o cadastro, contendo os dados básicos e foto.
<br><br>

O projeto foi desenvolvido usando as seguintes linguagens/frameworks:

- Linguagem Python
- Framework Django
- Banco de Dados MySql
- Bootstrap

<h1>Principais Funcionalidades</h1>

- Login
- Controle de acesso as telas por Grupo de Usuarios
- Upload de dados em lote em formato CSV
- Extração de Relátorios em CSV (Por data, Curso e Aluno)
- Logs de entrada/saida de alunos

