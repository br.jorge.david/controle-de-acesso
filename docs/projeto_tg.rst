projeto\_tg package
===================

Submodules
----------

projeto\_tg.asgi module
-----------------------

.. automodule:: projeto_tg.asgi
   :members:
   :undoc-members:
   :show-inheritance:

projeto\_tg.settings module
---------------------------

.. automodule:: projeto_tg.settings
   :members:
   :undoc-members:
   :show-inheritance:

projeto\_tg.urls module
-----------------------

.. automodule:: projeto_tg.urls
   :members:
   :undoc-members:
   :show-inheritance:

projeto\_tg.wsgi module
-----------------------

.. automodule:: projeto_tg.wsgi
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: projeto_tg
   :members:
   :undoc-members:
   :show-inheritance:
