catraca.migrations package
==========================

Submodules
----------

catraca.migrations.0001\_initial module
---------------------------------------

.. automodule:: catraca.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0002\_auto\_20200412\_1642 module
----------------------------------------------------

.. automodule:: catraca.migrations.0002_auto_20200412_1642
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0003\_auto\_20200412\_1953 module
----------------------------------------------------

.. automodule:: catraca.migrations.0003_auto_20200412_1953
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0004\_curso module
-------------------------------------

.. automodule:: catraca.migrations.0004_curso
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0005\_motivo module
--------------------------------------

.. automodule:: catraca.migrations.0005_motivo
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0006\_usuario module
---------------------------------------

.. automodule:: catraca.migrations.0006_usuario
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0007\_auto\_20200510\_1913 module
----------------------------------------------------

.. automodule:: catraca.migrations.0007_auto_20200510_1913
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0008\_auto\_20200510\_1918 module
----------------------------------------------------

.. automodule:: catraca.migrations.0008_auto_20200510_1918
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0009\_auto\_20200510\_1921 module
----------------------------------------------------

.. automodule:: catraca.migrations.0009_auto_20200510_1921
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0010\_auto\_20200510\_1929 module
----------------------------------------------------

.. automodule:: catraca.migrations.0010_auto_20200510_1929
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0011\_auto\_20200510\_2103 module
----------------------------------------------------

.. automodule:: catraca.migrations.0011_auto_20200510_2103
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0012\_usuario\_email module
----------------------------------------------

.. automodule:: catraca.migrations.0012_usuario_email
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0013\_auto\_20200511\_1314 module
----------------------------------------------------

.. automodule:: catraca.migrations.0013_auto_20200511_1314
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0014\_auto\_20200511\_1749 module
----------------------------------------------------

.. automodule:: catraca.migrations.0014_auto_20200511_1749
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0015\_auto\_20200511\_1917 module
----------------------------------------------------

.. automodule:: catraca.migrations.0015_auto_20200511_1917
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0016\_auto\_20200512\_2129 module
----------------------------------------------------

.. automodule:: catraca.migrations.0016_auto_20200512_2129
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0017\_auto\_20200512\_2144 module
----------------------------------------------------

.. automodule:: catraca.migrations.0017_auto_20200512_2144
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0018\_auto\_20200531\_1226 module
----------------------------------------------------

.. automodule:: catraca.migrations.0018_auto_20200531_1226
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0019\_remove\_inout\_codmotivo module
--------------------------------------------------------

.. automodule:: catraca.migrations.0019_remove_inout_codmotivo
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0020\_auto\_20200601\_1252 module
----------------------------------------------------

.. automodule:: catraca.migrations.0020_auto_20200601_1252
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0021\_inout\_codmotivo module
------------------------------------------------

.. automodule:: catraca.migrations.0021_inout_codmotivo
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0022\_inout\_coduser module
----------------------------------------------

.. automodule:: catraca.migrations.0022_inout_coduser
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0023\_auto\_20200608\_0925 module
----------------------------------------------------

.. automodule:: catraca.migrations.0023_auto_20200608_0925
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0024\_auto\_20200608\_0958 module
----------------------------------------------------

.. automodule:: catraca.migrations.0024_auto_20200608_0958
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0025\_auto\_20200608\_0959 module
----------------------------------------------------

.. automodule:: catraca.migrations.0025_auto_20200608_0959
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0026\_auto\_20200608\_1000 module
----------------------------------------------------

.. automodule:: catraca.migrations.0026_auto_20200608_1000
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0027\_auto\_20200608\_1013 module
----------------------------------------------------

.. automodule:: catraca.migrations.0027_auto_20200608_1013
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0028\_auto\_20200608\_1033 module
----------------------------------------------------

.. automodule:: catraca.migrations.0028_auto_20200608_1033
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0029\_auto\_20200608\_1102 module
----------------------------------------------------

.. automodule:: catraca.migrations.0029_auto_20200608_1102
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0030\_auto\_20200608\_1105 module
----------------------------------------------------

.. automodule:: catraca.migrations.0030_auto_20200608_1105
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0031\_auto\_20200608\_1109 module
----------------------------------------------------

.. automodule:: catraca.migrations.0031_auto_20200608_1109
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0032\_auto\_20200608\_1500 module
----------------------------------------------------

.. automodule:: catraca.migrations.0032_auto_20200608_1500
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0033\_auto\_20200608\_1502 module
----------------------------------------------------

.. automodule:: catraca.migrations.0033_auto_20200608_1502
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0034\_auto\_20200608\_1506 module
----------------------------------------------------

.. automodule:: catraca.migrations.0034_auto_20200608_1506
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0035\_remove\_pessoa\_foto module
----------------------------------------------------

.. automodule:: catraca.migrations.0035_remove_pessoa_foto
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0036\_pessoa\_foto module
--------------------------------------------

.. automodule:: catraca.migrations.0036_pessoa_foto
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0037\_auto\_20200608\_1511 module
----------------------------------------------------

.. automodule:: catraca.migrations.0037_auto_20200608_1511
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0038\_auto\_20200608\_1601 module
----------------------------------------------------

.. automodule:: catraca.migrations.0038_auto_20200608_1601
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0039\_auto\_20200608\_1610 module
----------------------------------------------------

.. automodule:: catraca.migrations.0039_auto_20200608_1610
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0040\_auto\_20200608\_1624 module
----------------------------------------------------

.. automodule:: catraca.migrations.0040_auto_20200608_1624
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0041\_auto\_20200608\_1711 module
----------------------------------------------------

.. automodule:: catraca.migrations.0041_auto_20200608_1711
   :members:
   :undoc-members:
   :show-inheritance:

catraca.migrations.0042\_auto\_20200614\_0913 module
----------------------------------------------------

.. automodule:: catraca.migrations.0042_auto_20200614_0913
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: catraca.migrations
   :members:
   :undoc-members:
   :show-inheritance:
