from django.conf.urls import url
from django.contrib import admin
from django.urls import path


from .views import *
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    #catraca
    #path('', exibe_catraca, name='catraca'),
    path('catraca', exibe_catraca, name='catraca'),
    path('sucesso', aluno_valido, name='sucesso'),
    path('falha', aluno_notfound, name='falha'),

    #usuarios
    path('listar_usuarios', listar_usuarios, name='listar_usuarios'),
    path('cadastrar_usuarios', cadastrar_usuario, name='cadastrar_usuarios'),
    path('logar_usuario', logar_usuario, name='logar_usuario'),
    path('deslogar_usuario', deslogar_usuario, name='deslogar_usuario'),
    #path('listar_usuarios/<int:id>', listar_usuario_id, name='listar_usuario_id'),
    #path('editar_usuarios/<int:id>', editar_usuario, name='editar_usuario'),
    #path('remover_usuarios/<int:id>', remover_usuario, name='remover_usuario'),

    #motivos
    path('listar_motivos', listar_motivos, name='listar_motivos'),
    path('cadastrar_motivos', inserir_motivo, name='cadastrar_motivos'),
    path('listar_motivos/<int:id>', listar_motivo_id, name='listar_motivo_id'),
    path('editar_motivos/<int:id>', editar_motivo, name='editar_motivo'),
    path('remover_motivos/<int:id>', remover_motivo, name='remover_motivo'),

    #pessoas
    path('listar', listar_pessoas, name='listar_pessoas'),
    path('cadastrar', inserir_pessoa, name='cadastrar_pessoas'),
    path('listar/<int:id>', listar_pessoa_id, name='listar_pessoa_id'),
    path('editar/<int:id>', editar_pessoa, name='editar_pessoa'),
    path('remover/<int:id>', remover_pessoa, name='remover_pessoa'),

    #cursos
    path('listar_cursos', listar_cursos, name='listar_cursos'),
    path('cadastrar_cursos', inserir_curso, name='cadastrar_cursos'),
    path('listar_cursos/<int:id>', listar_curso_id, name='listar_curso_id'),
    path('editar_cursos/<int:id>', editar_curso, name='editar_curso'),
    path('remover_cursos/<int:id>', remover_curso, name='remover_curso'),

    #relatorios
    path('pessoas_status', pessoas_status, name='pessoas_status'),
    path('pessoas_curso', pessoas_curso, name='pessoas_curso'),
    path('controle_acesso_cpf', controle_acesso_cpf, name='controle_acesso_cpf'),
    path('controle_acesso_ra', controle_acesso_ra, name='controle_acesso_ra'),
    path('controle_acesso_nome', controle_acesso_nome, name='controle_acesso_nome'),
    path('controle_acesso_geral', controle_acesso_geral, name='controle_acesso_geral'),
    path('import_csv_pessoas', import_csv_pessoas, name='import_csv_pessoas'),
]


