import csv
import io
from datetime import datetime

from django.db import IntegrityError
from django.utils import timezone
from validate_docbr import CPF
from validate_email import validate_email

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import PessoaForm, CursoForm, MotivoForm, UsuarioForm, UsuarioEditForm, CatracaForm, PessoaPorStatusForm, AlunoPorCursoForm, ControleAcessoCpfForm,ImportCsvPessoasForm, ControleAcessoRaForm, ControleAcessoNomeForm, PesquisaPessoasForm, ControleAcessoGeralForm
from .entidades import pessoa, curso, motivo, usuario, inout
from .models import Pessoa
from .services import pessoa_service, curso_service, motivo_service, usuario_service, inout_service
from django.contrib import messages
from bulk_update.helper import bulk_update

#catraca
#def exibe_catraca(request):
#    return render(request, 'entrada/catraca.html')

@login_required()
def exibe_catraca(request):
    if request.method == 'POST':
        form = CatracaForm(request.POST)

        if form.is_valid():
            ra = form.cleaned_data["ra"]
            motivo = form.cleaned_data["motivo"]
            user = request.user
            pessoa = pessoa_service.listar_pessoa_ra(ra)
            if pessoa is None:
                return redirect('falha')
            else:
                if user is None:
                    user = 0
                inout_service.inserir_registro(pessoa, motivo, user)
                return render(request, 'entrada/sucesso.html', {'pessoa': pessoa})
    else:
        form = CatracaForm()
    return render(request, 'entrada/catraca.html', {'form': form})

@login_required()
def aluno_valido(request):
    #usuarios = usuario_service.listar_usuarios()
    return render(request,'entrada/sucesso.html', {'usuarios': 'sucesso!'})

@login_required()
def aluno_notfound(request):
    #usuarios = usuario_service.listar_usuarios()
    return render(request,'entrada/falha.html', {'usuarios': 'Erro!'})


#Usuarios
@login_required()
def listar_usuarios(request):
    usuarios = usuario_service.listar_usuarios()
    return render(request,'usuarios/lista_usuarios.html', {'usuarios': usuarios})

@login_required()
def inserir_usuario(request):
    if request.method == "POST":
        form = UsuarioForm(request.POST)
        if form.is_valid():
            nome = form.cleaned_data["nome"]
            email = form.cleaned_data["email"]
            user = form.cleaned_data["user"]
            cpf = form.cleaned_data["cpf"]
            rg = form.cleaned_data["rg"]
            grupo = form.cleaned_data["grupo"]
            status = form.cleaned_data["status"]
            senha = form.cleaned_data["senha"]

            usuario_novo = usuario.Usuario(nome=nome, user=user, cpf=cpf, rg=rg, grupo=grupo, status=status, senha=senha, email = email)
            usuario_service.cadastrar_usuario(usuario_novo)

            return redirect ('listar_usuarios')
    else:
        form = UsuarioForm()
    return render(request, 'usuarios/form_usuario.html', {'form': form})

@login_required()
def listar_usuario_id(request, id):
    usuario = usuario_service.listar_usuario_id(id)
    return render(request, 'usuarios/lista_usuario.html', {'usuario': usuario})

@login_required()
def editar_usuario(request, id):
    usuario_antigo = usuario_service.listar_usuario_id(id)
    form = UsuarioEditForm(request.POST or None, instance=usuario_antigo)
    if form.is_valid():
        nome = form.cleaned_data["nome"]
        email = form.cleaned_data["email"]
        user = form.cleaned_data["user"]
        cpf = form.cleaned_data["cpf"]
        rg = form.cleaned_data["rg"]
        grupo = form.cleaned_data["grupo"]
        status = form.cleaned_data["status"]

        usuario_novo = usuario.Usuario(nome=nome, user = user, cpf = cpf, rg = rg, grupo = grupo, status = status, email = email, senha = usuario_antigo.senha)
        usuario_service.editar_usuario(usuario_antigo, usuario_novo)
        return redirect('listar_usuarios')

    return render(request, 'usuarios/form_usuario_edit.html', {'form': form})

@login_required()
def remover_usuario(request, id):
    usuario = usuario_service.listar_usuario_id(id)
    if request.method == "POST":
        usuario_service.remover_usuario(usuario)
        return redirect('listar_usuarios')
    return render(request, 'usuarios/confirma_exclusao.html', {'usuario': usuario})


#Motivos
@login_required()
def listar_motivos(request):
    motivos = motivo_service.listar_motivos()
    return render(request,'motivos/lista_motivos.html', {'motivos': motivos})

@login_required()
def inserir_motivo(request):
    if request.method == "POST":
        form = MotivoForm(request.POST)
        if form.is_valid():
            descrmotivo = form.cleaned_data["descrmotivo"]
            obs = form.cleaned_data["obs"]

            motivo_novo = motivo.Motivo(descrmotivo=descrmotivo, obs=obs)
            motivo_service.cadastrar_motivo(motivo_novo)

            return redirect ('listar_motivos')
    else:
        form = MotivoForm()
    return render(request, 'motivos/form_motivo.html', {'form': form})

@login_required()
def listar_motivo_id(request, id):
    motivo = motivo_service.listar_motivo_id(id)
    return render(request, 'motivos/lista_motivo.html', {'motivo': motivo})

@login_required()
def editar_motivo(request, id):
    motivo_antigo = motivo_service.listar_motivo_id(id)
    form = MotivoForm(request.POST or None, instance=motivo_antigo)
    if form.is_valid():
        descrmotivo = form.cleaned_data["descrmotivo"]
        obs = form.cleaned_data["obs"]

        motivo_novo = motivo.Motivo(descrmotivo=descrmotivo, obs=obs)
        motivo_service.editar_motivo(motivo_antigo, motivo_novo)
        return redirect('listar_motivos')
    return render(request, 'motivos/form_motivo.html', {'form': form})

@login_required()
def remover_motivo(request, id):
    motivo = motivo_service.listar_motivo_id(id)
    if request.method == "POST":
        motivo_service.remover_motivo(motivo)
        return redirect('listar_motivos')
    return render(request, 'motivos/confirma_exclusao.html', {'motivo': motivo})

#Cursos
@login_required()
def listar_cursos(request):
    cursos = curso_service.listar_cursos()
    return render(request,'cursos/lista_cursos.html', {'cursos': cursos})

@login_required()
def inserir_curso(request):
    if request.method == "POST":
        form = CursoForm(request.POST)
        if form.is_valid():
            descrcurso = form.cleaned_data["descrcurso"]

            curso_novo = curso.Curso(descrcurso=descrcurso)
            curso_service.cadastrar_curso(curso_novo)

            return redirect ('listar_cursos')
    else:
        form = CursoForm()
    return render(request, 'cursos/form_curso.html', {'form': form})

@login_required()
def listar_curso_id(request, id):
    curso = curso_service.listar_curso_id(id)
    return render(request, 'cursos/lista_curso.html', {'curso': curso})

@login_required()
def editar_curso(request, id):
    curso_antigo = curso_service.listar_curso_id(id)
    form = CursoForm(request.POST or None, instance=curso_antigo)
    if form.is_valid():
        descrcurso = form.cleaned_data["descrcurso"]
        curso_novo = curso.Curso(descrcurso=descrcurso)
        curso_service.editar_curso(curso_antigo, curso_novo)
        return redirect('listar_cursos')
    return render(request, 'cursos/form_curso.html', {'form': form})

@login_required()
def remover_curso(request, id):
    curso = curso_service.listar_curso_id(id)
    if request.method == "POST":
        curso_service.remover_curso(curso)
        return redirect('listar_cursos')
    return render(request, 'cursos/confirma_exclusao.html', {'curso': curso})

@login_required()
# Pessoas
def listar_pessoas(request):
    if request.method == "POST":
        form = PesquisaPessoasForm(request.POST)
        if form.is_valid():
            nome = form.cleaned_data["nome"]
            pessoas = pessoa_service.pessoa_por_nome_ra(nome)
            return render(request,'pessoas/lista_pessoas.html', {'pessoas': pessoas, 'form': form})
    else:
        form = PesquisaPessoasForm()
        pessoas = pessoa_service.listar_pessoas()
        people = Pessoa.objects.all()
        for pessoa in people:
            print(pessoa.foto)
            if pessoa.foto == '':
                pessoa.foto = 'produtos/avatar.png'
        bulk_update(people)
        return render(request,'pessoas/lista_pessoas.html', {'pessoas': pessoas, 'form':form})

@login_required()
def inserir_pessoa(request):
    if request.method == "POST":
        form = PessoaForm(request.POST, request.FILES)
        if form.is_valid():
            nome = form.cleaned_data["nome"]
            curso = form.cleaned_data["curso"]
            sexo = form.cleaned_data["sexo"]
            tipo = form.cleaned_data["tipo"]
            ra = form.cleaned_data["ra"]
            cpf = form.cleaned_data["cpf"]
            rg = form.cleaned_data["rg"]
            email = form.cleaned_data["email"]
            telefone_fixo = form.cleaned_data["telefone_fixo"]
            telefone_movel = form.cleaned_data["telefone_movel"]
            status = form.cleaned_data["status"]
            foto = form.cleaned_data["foto"]


            pessoa_novo = pessoa.Pessoa(nome=nome, ra=ra, cpf=cpf, rg=rg, email=email, status=status, tipo=tipo, sexo=sexo, telefone_fixo = telefone_fixo, telefone_movel = telefone_movel, curso = curso, foto=foto)
            pessoa_service.cadastrar_pessoa(pessoa_novo)

            return redirect ('listar_pessoas')
        else:
            print('Nao valido form')
    else:
        form = PessoaForm()
    return render(request, 'pessoas/form_pessoa.html', {'form': form})

@login_required()
def listar_pessoa_id(request, id):
    pessoa = pessoa_service.listar_pessoa_id(id)
    return render(request, 'pessoas/lista_pessoa.html', {'pessoa': pessoa})

@login_required()
def editar_pessoa(request, id):
    pessoa_antigo = pessoa_service.listar_pessoa_id(id)
    form = PessoaForm(request.POST or None, request.FILES or None, instance=pessoa_antigo)
    if form.is_valid():
        nome = form.cleaned_data["nome"]
        curso = form.cleaned_data["curso"]
        sexo = form.cleaned_data["sexo"]
        tipo = form.cleaned_data["tipo"]
        ra = form.cleaned_data["ra"]
        cpf = form.cleaned_data["cpf"]
        rg = form.cleaned_data["rg"]
        email = form.cleaned_data["email"]
        telefone_fixo = form.cleaned_data["telefone_fixo"]
        telefone_movel = form.cleaned_data["telefone_movel"]
        status = form.cleaned_data["status"]
        foto = form.cleaned_data["foto"]

        pessoa_novo = pessoa.Pessoa(nome=nome, ra=ra, cpf=cpf, rg=rg, email=email, status=status, tipo=tipo, sexo=sexo,
                                    telefone_fixo=telefone_fixo, telefone_movel=telefone_movel, curso = curso, foto = foto)
        pessoa_service.editar_pessoa(pessoa_antigo, pessoa_novo)
        return redirect('listar_pessoas')
    return render(request, 'pessoas/form_pessoa.html', {'form': form})

@login_required()
def remover_pessoa(request, id):
    pessoa = pessoa_service.listar_pessoa_id(id)
    if request.method == "POST":
        pessoa_service.remover_pessoa(pessoa)
        return redirect('listar_pessoas')
    return render(request, 'pessoas/confirma_exclusao.html', {'pessoa': pessoa})


def cadastrar_usuario(request):
    if request.method == "POST":
        form_usuario = UserCreationForm(request.POST)
        if form_usuario.is_valid():
            form_usuario.save()
            return redirect('listar_pessoas')
    else:
        form_usuario = UserCreationForm()
    return render(request, 'usuarios/form_usuario.html', {"form_usuario": form_usuario})


def logar_usuario(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        usuario = authenticate(request, username=username, password=password)
        if usuario is not None:
            login(request, usuario)
            return redirect('catraca')
        else:
            messages.error(request, 'As credenciais do usuario estão incorretas!')
            return redirect('logar_usuario')
    else:
        form_login = AuthenticationForm()
    return render(request, 'login/login.html', {"form_login": form_login})

def deslogar_usuario(request):
    logout(request)
    return redirect('logar_usuario')

#relatorio listar todos os alunos cadastrados
def pessoas_status(request):
    if request.method == 'POST':
        form = PessoaPorStatusForm(request.POST)
        if form.is_valid():
            status = form.cleaned_data["status"]
            dados = pessoa_service.pessoa_status(status)
            # Create the HttpResponse object with the appropriate CSV header.
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
            print ('Valido')
            writer = csv.writer(response)
            writer.writerow(['Nome', 'Curso', 'RA', 'CPF', 'RG', 'Email', 'Telefone Fixo', 'Telefone Celular', 'Status', 'Sexo', 'Tipo'])
            for dado in dados:
                writer.writerow([dado.nome, dado.curso, dado.ra, dado.cpf, dado.rg, dado.email, dado.telefone_fixo, dado.telefone_movel, dado.status, dado.sexo, dado.tipo])
            return response
    else:
        form = PessoaPorStatusForm()
    return render(request, 'relatorios/pessoas_status.html', {'form': form})

#relatorio listar todos os alunos cadastrados em um curso
def pessoas_curso(request):
    if request.method == 'POST':
        form = AlunoPorCursoForm(request.POST)
        if form.is_valid():
            curso = form.cleaned_data["curso"]
            pessoas = pessoa_service.pessoa_curso(curso)
            # Create the HttpResponse object with the appropriate CSV header.
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="pessoas_curso.csv"'
            writer = csv.writer(response)
            writer.writerow(['Nome', 'Curso', 'RA', 'CPF', 'RG', 'Email', 'Telefone Fixo', 'Telefone Celular', 'Status', 'Sexo','Tipo'])
            for pessoa in pessoas:
                writer.writerow([pessoa.nome, pessoa.curso, pessoa.ra, pessoa.cpf, pessoa.rg, pessoa.email, pessoa.telefone_fixo, pessoa.telefone_movel, pessoa.status, pessoa.sexo, pessoa.tipo])
            return response
    else:
        form = AlunoPorCursoForm()
    return render(request, 'relatorios/pessoas_curso.html', {'form': form})

#relatorio para gerar na tela os registros de controle de acesso por CPF/Data
def controle_acesso_cpf(request):
    if request.method == 'POST':
        form = ControleAcessoCpfForm(request.POST)
        if form.is_valid():
            dataini = form.cleaned_data["dataini"]
            datafin = form.cleaned_data["datafin"]
            cpf = form.cleaned_data["cpf"]
            exibicao = form.cleaned_data["exibicao"]
            if (exibicao == 'Tela'):
                registros = inout_service.consulta_entrada_cpf(dataini, datafin, cpf)
                return render(request, 'relatorios/registros_pesquisa.html', {'registros': registros, 'dataini': dataini, 'datafin': datafin})
            if (exibicao == 'CSV'):
                data_e_hora_atuais = datetime.now()
                slc = slice(19)
                data_e_hora_em_texto = data_e_hora_atuais.strftime("%d_%m_%Y_%H_%M_%S")
                agora = str(data_e_hora_atuais).replace("-", "_")
                agora = agora.replace(" ", "_")
                agora = agora.replace(":", "_")
                agora = agora[slc]
                registros = inout_service.consulta_entrada_cpf(dataini, datafin, cpf)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="registros_por_cpf_' + agora + '.csv"'
                writer = csv.writer(response)
                writer.writerow(
                    ['Data Ocorrencia', 'Pessoa', 'Ra', 'CPF', 'RG', 'Curso', 'Motivo', 'Usuario Portaria'])
                for registro in registros:
                    writer.writerow([registro.dtocorrencia, registro.codpessoa.nome,registro.codpessoa.ra,registro.codpessoa.cpf,registro.codpessoa.rg,registro.codpessoa.curso, registro.codmotivo, registro.coduser])
                return response
    else:
        form = ControleAcessoCpfForm()
    return render(request, 'relatorios/controle_acesso_cpf.html', {'form': form})

#relatorio para gerar na tela os registros de controle de acesso por Nome/Data
def controle_acesso_nome(request):
    if request.method == 'POST':
        form = ControleAcessoNomeForm(request.POST)
        if form.is_valid():
            dataini = form.cleaned_data["dataini"]
            datafin = form.cleaned_data["datafin"]
            nome = form.cleaned_data["nome"]
            exibicao = form.cleaned_data["exibicao"]
            if (exibicao == 'Tela'):
                registros = inout_service.consulta_entrada_nome(dataini, datafin, nome)
                return render(request, 'relatorios/registros_pesquisa.html', {'registros': registros, 'dataini': dataini, 'datafin': datafin})
            if (exibicao == 'CSV'):
                data_e_hora_atuais = datetime.now()
                slc = slice(19)
                data_e_hora_em_texto = data_e_hora_atuais.strftime("%d_%m_%Y_%H_%M_%S")
                agora = str(data_e_hora_atuais).replace("-", "_")
                agora = agora.replace(" ", "_")
                agora = agora.replace(":", "_")
                agora = agora[slc]
                registros = inout_service.consulta_entrada_nome(dataini, datafin, nome)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="registros_por_nome_' + agora + '.csv"'
                writer = csv.writer(response)
                writer.writerow(
                    ['Data Ocorrencia', 'Pessoa', 'Ra', 'CPF', 'RG', 'Curso', 'Motivo', 'Usuario Portaria'])
                for registro in registros:
                    writer.writerow([registro.dtocorrencia, registro.codpessoa.nome,registro.codpessoa.ra,registro.codpessoa.cpf,registro.codpessoa.rg,registro.codpessoa.curso, registro.codmotivo, registro.coduser])
                return response
    else:
        form = ControleAcessoNomeForm()
    return render(request, 'relatorios/controle_acesso_nome.html', {'form': form})

#controle de acesos geral
def controle_acesso_geral(request):
    if request.method == 'POST':
        form = ControleAcessoGeralForm(request.POST)
        if form.is_valid():
            dataini = form.cleaned_data["dataini"]
            datafin = form.cleaned_data["datafin"]
            exibicao = form.cleaned_data["exibicao"]
            if (exibicao == 'Tela'):
                registros = inout_service.consulta_entrada_nome(dataini, datafin)
                return render(request, 'relatorios/registros_pesquisa.html', {'registros': registros, 'dataini': dataini, 'datafin': datafin})
            if (exibicao == 'CSV'):
                data_e_hora_atuais = datetime.now()
                slc = slice(19)
                data_e_hora_em_texto = data_e_hora_atuais.strftime("%d_%m_%Y_%H_%M_%S")
                agora = str(data_e_hora_atuais).replace("-", "_")
                agora = agora.replace(" ", "_")
                agora = agora.replace(":", "_")
                agora = agora[slc]
                registros = inout_service.consulta_entrada_nome(dataini, datafin)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="registros_por_nome_' + agora + '.csv"'
                writer = csv.writer(response)
                writer.writerow(
                    ['Data Ocorrencia', 'Pessoa', 'Ra', 'CPF', 'RG', 'Curso', 'Motivo', 'Usuario Portaria'])
                for registro in registros:
                    writer.writerow([registro.dtocorrencia, registro.codpessoa.nome,registro.codpessoa.ra,registro.codpessoa.cpf,registro.codpessoa.rg,registro.codpessoa.curso, registro.codmotivo, registro.coduser])
                return response
    else:
        form = ControleAcessoNomeForm()
    return render(request, 'relatorios/controle_acesso_nome.html', {'form': form})

def import_csv(filename: str):
    with open(filename) as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',')
        csv_data = [line for line in reader]
    imprime_dados_csv(csv_data)
    return csv_data

#relatorio para gerar na tela os registros de controle de acesso por RA/Data
def controle_acesso_ra(request):
    if request.method == 'POST':
        form = ControleAcessoRaForm(request.POST)
        if form.is_valid():
            dataini = form.cleaned_data["dataini"]
            datafin = form.cleaned_data["datafin"]
            ra = form.cleaned_data["ra"]
            exibicao = form.cleaned_data["exibicao"]
            if (exibicao == 'Tela'):
                registros = inout_service.consulta_entrada_ra(dataini, datafin, ra)
                return render(request, 'relatorios/registros_pesquisa.html', {'registros': registros, 'dataini': dataini, 'datafin': datafin})
            if (exibicao == 'CSV'):
                data_e_hora_atuais = datetime.now()
                slc = slice(19)
                data_e_hora_em_texto = data_e_hora_atuais.strftime("%d_%m_%Y_%H_%M_%S")
                agora = str(data_e_hora_atuais).replace("-", "_")
                agora = agora.replace(" ", "_")
                agora = agora.replace(":", "_")
                agora = agora[slc]
                #print ('Hora: ' + agora)
                registros = inout_service.consulta_entrada_ra(dataini, datafin, ra)
                response = HttpResponse(content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename="registros_por_ra_' + agora +'.csv"'
                writer = csv.writer(response)
                writer.writerow(
                    ['Data Ocorrencia', 'Pessoa', 'Ra', 'CPF', 'RG', 'Curso', 'Motivo', 'Usuario Portaria'])
                for registro in registros:
                    writer.writerow([registro.dtocorrencia, registro.codpessoa.nome,registro.codpessoa.ra,registro.codpessoa.cpf,registro.codpessoa.rg,registro.codpessoa.curso, registro.codmotivo, registro.coduser])
                return response
    else:
        form = ControleAcessoRaForm()
    return render(request, 'relatorios/controle_acesso_ra.html', {'form': form})

def import_csv(filename: str):
    with open(filename) as csv_file:
        reader = csv.DictReader(csv_file, delimiter=',')
        csv_data = [line for line in reader]
    imprime_dados_csv(csv_data)
    return csv_data

def imprime_dados_csv(csv):
    aux = []
    for item in csv:
        nome = item.get('nome')
        sexo = item.get('sexo')
        curso = item.get('curso')
        tipo = item.get('tipo')
        ra = item.get('ra')
        cpf = item.get('cpf')

        obj = Pessoa(nome=nome, sexo=sexo, curso=curso, tipo=tipo, ra=ra, cpf=cpf)
        aux.append(obj)
        print ('Listando...' + aux)
    #Pessoa.objects.bulk_create(aux)

#verificando se o campo é um numero
def isnumber(value):
    try:
         int(value)
    except ValueError:
         return False
    return True

#validando o email
def isemail(value):
    is_valid = validate_email(value)
    return is_valid

#validando cpf
def valida_cpf(cpf):
    valid_cpf = CPF()
    isvalid = valid_cpf.validate(cpf)
    return isvalid

#verificando a existencia de pontos
def verifica_pontos(valor):
    r = valor.find('.')
    return r

def import_csv_pessoas(request):
    # Handle file upload
    if request.method == 'POST':
        aux=[]
        lista = []
        lista_erros = []
        erros = ''
        count = 0
        qtd_erros = 0
        form = ImportCsvPessoasForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = request.FILES['docfile']
            data_set = csv_file.read().decode('UTF-8')
            io_string = io.StringIO(data_set)
            next(io_string)

            for column in csv.reader(io_string, delimiter= ',', quotechar="|"):
                count += 1;
                nome=column[0]
                sexo=column[1]
                ra=column[2]
                cpf=column[3]
                rg=column[4]
                email=column[5]
                telefone_fixo=column[6]
                telefone_movel=column[7]
                tipo=column[8]
                status=column[9]
                curso=column[10]

                if curso_service.listar_curso_id(curso) is None:
                    erros = 'Linha ' + str(count) + ': Curso não encontrado.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if pessoa_service.listar_pessoa_cpf(cpf) is not None:
                    erros = 'Linha ' + str(count) + ': CPF já cadastrado!'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if pessoa_service.listar_pessoa_rg(rg) is not None:
                    erros = 'Linha ' + str(count) + ': RG já cadastrado!'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if pessoa_service.listar_pessoa_ra(ra) is not None:
                    erros = 'Linha ' + str(count) + ': RA já cadastrado!'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if sexo not in ('M', 'F', 'N'):
                    erros = 'Linha ' + str(count) + ': Sexo Invalido. Os valores aceitos são: M, F ou N (M = Masculino, F=Feminino, N=Não definido).'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if tipo not in ('A', 'V', 'F'):
                    erros = 'Linha ' + str(count) + ': Tipo Invalido. Os valores aceitos são: A, V ou F (A=Aluno, V=Visitante, F=Funcionario).'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if status not in ('A', 'I'):
                    erros = 'Linha ' + str(count) + ': Status Invalido. Os valores aceitos são: A ou I (A=Ativo, I=Inativo).'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if len(telefone_fixo)  != 10:
                    erros = 'Linha ' + str(count) + ': Telefone Fixo Invalido. Deve ter 10 caracteres.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if isnumber (telefone_fixo) is False:
                    erros = 'Linha ' + str(count) + ': Telefone Fixo Invalido. Deve conter apenas numeros.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if verifica_pontos(telefone_fixo)  != -1:
                    erros = 'Linha ' + str(count) + ': Telefone Fixo não pode conter pontos.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if len(telefone_movel) != 11:
                    erros = 'Linha ' + str(count) + ': Telefone Celular Invalido. Deve ter 11 caracteres.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if isnumber(telefone_movel) is False:
                    erros = 'Linha ' + str(count) + ': Telefone Celular Invalido. Deve conter apenas numeros.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if isnumber(ra) is False:
                    erros = 'Linha ' + str(count) + ': RA Invalido. Deve conter apenas numeros.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if verifica_pontos(ra) != -1:
                    erros = 'Linha ' + str(count) + ':  RA não pode conter pontos.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if verifica_pontos(telefone_movel) != -1:
                    erros = 'Linha ' + str(count) + ':  Telefone Celular não pode conter pontos.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if isemail(email) is False:
                    erros = 'Linha ' + str(count) + ': Email invalido Invalido.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if valida_cpf(cpf) is False:
                    erros = 'Linha ' + str(count) + ': CPF invalido.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if verifica_pontos(cpf) != -1:
                    erros = 'Linha ' + str(count) + ': CPF não pode conter pontos.'
                    lista_erros.append(erros)
                    qtd_erros += 1

                if isnumber(rg) is False:
                    erros = 'Linha ' + str(count) + ': RG Invalido. Deve conter apenas numeros.'
                    lista_erros.append(erros)
                    qtd_erros += 1


                obj = Pessoa(nome=nome, sexo=sexo, ra=ra,cpf=cpf, rg=rg, email=email, telefone_fixo=telefone_fixo, telefone_movel=telefone_movel, tipo=tipo, status=status, curso_id=curso)
                aux.append(obj)

            if qtd_erros == 0:
                try:
                    Pessoa.objects.bulk_create(aux)
                except IntegrityError as e:
                    erros = 'Problema ao tentar inserir registros no banco de dados. Provavelmente exista algum dado repetido!'
                    lista_erros.append(erros)
                    lista_erros.append(e.__cause__)
                    qtd_erros += 1
                    return render(request, 'relatorios/list_csv.html', {'lista': aux, 'erros': lista_erros, 'qtderro': qtd_erros})
                #pessoas = pessoa_service.listar_pessoas()
                sucesso = 'Importação realizado com sucesso!'
                return render(request, 'relatorios/sucesso_import_csv.html', {'sucesso': sucesso})
            else:
                lista_erros.append('Erro durante o insert dos dados!')
            return render(request, 'relatorios/list_csv.html', {'lista': aux, 'erros': lista_erros, 'qtderro': qtd_erros})
    else:
        form = ImportCsvPessoasForm() # A empty, unbound form

    return render(request, 'relatorios/import_csv.html', {'form': form})
