from ..models import Pessoa
from django.db.models import Q

def listar_pessoas():
    pessoas = Pessoa.objects.all().order_by('nome', 'curso')
    return pessoas

def listar_pessoa_id(id):
    pessoa = Pessoa.objects.get(id=id)
    return pessoa

def listar_pessoa_ra(ra):
    try:
        pessoa = Pessoa.objects.get(ra=ra, status = 'A')
    except Pessoa.DoesNotExist:
        pessoa = None

    return pessoa


def listar_todos_pessoa_ra(ra):
    try:
        pessoa = Pessoa.objects.get(ra=ra)
    except Pessoa.DoesNotExist:
        pessoa = None
    return pessoa

def listar_pessoa_sem_foto():
    try:
        pessoa = Pessoa.objects.filter(foto='')
    except Pessoa.DoesNotExist:
        pessoa = None
    return pessoa

def listar_pessoa_cpf(cpf):
    try:
        pessoa = Pessoa.objects.get(cpf=cpf)
        print('entrou')
        print (pessoa.id)
    except Pessoa.DoesNotExist:
        pessoa = None
    return pessoa

def listar_pessoa_rg(rg):
    try:
        pessoa = Pessoa.objects.get(rg=rg)
    except Pessoa.DoesNotExist:
        pessoa = None

    return pessoa

def id_pessoa(ra):
    try:
        pessoa = Pessoa.objects.get(ra=ra)
    except Pessoa.DoesNotExist:
        pessoa = None
    return pessoa.id

def pessoa_curso(curso):
    try:
        pessoas = Pessoa.objects.filter(curso=curso)
    except Pessoa.DoesNotExist:
        pessoa = None
    return pessoas

def pessoa_status(status):
    try:
        if status == 'T':
            pessoas = Pessoa.objects.all()

        if status == 'A' or status == 'I':
            pessoas = Pessoa.objects.filter(status=status)
    except Pessoa.DoesNotExist:
        pessoa = None
    return pessoas

def pessoa_por_nome(nome):
    try:
        if nome is not None:
            pessoas = Pessoa.objects.filter(nome__icontains=nome)
            #pessoas = Pessoa.objects.filter(Q(nome__icontains=nome) | Q(ra__icontains=nome))
    except Pessoa.DoesNotExist:
        pessoa = None
    #print ('pessoas')
    #print (pessoas)
    return pessoas

def pessoa_por_nome_ra(nome):
    try:
        if nome is not None:
            #pessoas = Pessoa.objects.filter(nome__icontains=nome)
            pessoas = Pessoa.objects.filter(Q(nome__icontains=nome) | Q(ra__icontains=nome))
    except Pessoa.DoesNotExist:
        pessoa = None
    #print ('pessoas')
    #print (pessoas)
    return pessoas

def remover_pessoa(pessoa):
    pessoa.delete()

def cadastrar_pessoa(pessoa):
    Pessoa.objects.create(nome=pessoa.nome, ra=pessoa.ra, sexo=pessoa.sexo, status=pessoa.status, tipo=pessoa.tipo, telefone_fixo=pessoa.telefone_fixo, telefone_movel=pessoa.telefone_movel, rg=pessoa.rg, cpf=pessoa.cpf, email=pessoa.email, curso = pessoa.curso, foto = pessoa.foto)
    print (pessoa.foto)

def editar_pessoa(pessoa, pessoa_novo):
    pessoa.nome = pessoa_novo.nome
    pessoa.email = pessoa_novo.email
    pessoa.sexo = pessoa_novo.sexo
    pessoa.status = pessoa_novo.status
    pessoa.tipo = pessoa_novo.tipo
    pessoa.telefone_fixo = pessoa_novo.telefone_fixo
    pessoa.telefone_movel = pessoa_novo.telefone_movel
    pessoa.rg = pessoa_novo.rg
    pessoa.ra = pessoa_novo.ra
    pessoa.cpf = pessoa_novo.cpf
    pessoa.curso = pessoa_novo.curso
    pessoa.foto = pessoa_novo.foto
    pessoa.save(force_update=True)
