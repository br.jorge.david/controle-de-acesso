from ..models import Usuario

def listar_usuarios():
    usuarios = Usuario.objects.all()
    return usuarios

def listar_usuario_id(id):
    usuario = Usuario.objects.get(id=id)
    return usuario

def remover_usuario(usuario):
    usuario.delete()

def cadastrar_usuario(usuario):
    Usuario.objects.create(nome = usuario.nome, user = usuario.user, cpf = usuario.cpf, rg = usuario.rg, grupo = usuario.grupo, status = usuario.status, senha = usuario.senha, email = usuario.email)

def editar_usuario(usuario, usuario_novo):
    usuario.nome = usuario_novo.nome
    usuario.email = usuario_novo.email
    usuario.usuario = usuario_novo.user
    usuario.cpf = usuario_novo.cpf
    usuario.grupo = usuario_novo.grupo
    usuario.status = usuario_novo.status
    usuario.senha = usuario_novo.senha

    usuario.save(force_update=True)
