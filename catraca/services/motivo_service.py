from ..models import Motivo

def listar_motivos():
    motivos = Motivo.objects.all()
    return motivos

def listar_motivo_id(id):
    motivo = Motivo.objects.get(id=id)
    return motivo

def remover_motivo(motivo):
    motivo.delete()

def cadastrar_motivo(motivo):
    Motivo.objects.create(descrmotivo=motivo.descrmotivo, obs=motivo.obs)

def editar_motivo(motivo, motivo_novo):
    motivo.descrmotivo = motivo_novo.descrmotivo
    motivo.obs = motivo_novo.obs
    motivo.save(force_update=True)
