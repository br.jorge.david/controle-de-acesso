from datetime import datetime as dt
import datetime
from django.contrib.auth.models import User

from . import pessoa_service
from ..models import INOUT, Pessoa, Motivo


def listar_inout():
    registros = INOUT.objects.all()
    return registros

def listar_registro(codpessoa):
    registros = INOUT.objects.get(codpessoa=codpessoa)
    return registros

def inserir_registro(pessoa, motivo, user):
    p = Pessoa(id=pessoa.id)
    m = Motivo(id = motivo.id)
    u = User(id = user.id)
    INOUT.objects.create(codpessoa = p, codmotivo = m, coduser = u)

def consulta_entrada_cpf(datain, datafin, cpf):
    id_pessoa = pessoa_service.listar_pessoa_cpf(cpf)
    from_date_ini = dt.strptime(datain, "%d/%m/%Y").date()
    from_date_fin = dt.strptime(datafin, "%d/%m/%Y").date()
    from_date_ini = datetime.datetime.combine(from_date_ini, datetime.time.min)
    from_date_fin = datetime.datetime.combine(from_date_fin, datetime.time.max)
    try:
        registros = INOUT.objects.filter(dtocorrencia__range=[from_date_ini, from_date_fin], codpessoa_id=id_pessoa)
    except INOUT.DoesNotExist:
        registros = None
    return registros

def consulta_entrada_nome(datain, datafin, nome):
    lista = []
    pessoas = pessoa_service.pessoa_por_nome(nome)
    from_date_ini = dt.strptime(datain, "%d/%m/%Y").date()
    from_date_fin = dt.strptime(datafin, "%d/%m/%Y").date()
    from_date_ini = datetime.datetime.combine(from_date_ini, datetime.time.min)
    from_date_fin = datetime.datetime.combine(from_date_fin, datetime.time.max)

    for pessoa in pessoas:
        try:
            registros = INOUT.objects.filter(dtocorrencia__range=[from_date_ini, from_date_fin], codpessoa_id=pessoa.id)
            for i in registros:
                #print ('registros')
                #print (i.codpessoa_id)
                lista.append(i)
        except INOUT.DoesNotExist:
            registros = None
    #for a in lista:
        #print('lista')
        #print(a.codpessoa_id)
    return lista

def consulta_entrada_geral(datain, datafin):
    lista = []
    from_date_ini = dt.strptime(datain, "%d/%m/%Y").date()
    from_date_fin = dt.strptime(datafin, "%d/%m/%Y").date()
    from_date_ini = datetime.datetime.combine(from_date_ini, datetime.time.min)
    from_date_fin = datetime.datetime.combine(from_date_fin, datetime.time.max)
    try:
        registros = INOUT.objects.filter(dtocorrencia__range=[from_date_ini, from_date_fin])
    except INOUT.DoesNotExist:
        registros = None
    return registros

def consulta_entrada_ra(datain, datafin, ra):
    id_pessoa = pessoa_service.id_pessoa(ra)
    print ('ra: ' + str(id_pessoa))
    from_date_ini = dt.strptime(datain, "%d/%m/%Y").date()
    from_date_fin = dt.strptime(datafin, "%d/%m/%Y").date()
    from_date_ini = datetime.datetime.combine(from_date_ini, datetime.time.min)
    from_date_fin = datetime.datetime.combine(from_date_fin, datetime.time.max)
    try:
        registros = INOUT.objects.filter(dtocorrencia__range=[from_date_ini, from_date_fin], codpessoa_id=id_pessoa)
    except INOUT.DoesNotExist:
        registros = None
    return registros

