from ..models import Curso

def listar_cursos():
    cursos = Curso.objects.all()
    return cursos

def listar_curso_id(id):
    try:
        curso = Curso.objects.get(id=id)
    except Curso.DoesNotExist:
        curso = None
    return curso

def remover_curso(curso):
    curso.delete()

def cadastrar_curso(curso):
    Curso.objects.create(descrcurso=curso.descrcurso)

def editar_curso(curso, curso_novo):
    curso.descrcurso = curso_novo.descrcurso
    curso.save(force_update=True)
