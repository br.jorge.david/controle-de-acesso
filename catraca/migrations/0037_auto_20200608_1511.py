# Generated by Django 3.0.6 on 2020-06-08 18:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catraca', '0036_pessoa_foto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='foto',
            field=models.FileField(default=None, null=True, upload_to='%Y/%m/%d/'),
        ),
    ]
