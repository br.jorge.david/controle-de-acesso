# Generated by Django 3.0.5 on 2020-04-12 19:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catraca', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='tipo',
            field=models.CharField(choices=[('A', 'Aluno'), ('V', 'Visitante'), ('F', 'Funcionario')], max_length=1),
        ),
    ]
