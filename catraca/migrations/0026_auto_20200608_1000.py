# Generated by Django 3.0.6 on 2020-06-08 13:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catraca', '0025_auto_20200608_0959'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='media'),
        ),
    ]
