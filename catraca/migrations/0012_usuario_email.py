# Generated by Django 3.0.6 on 2020-05-11 03:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catraca', '0011_auto_20200510_2103'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='email',
            field=models.EmailField(default='contato@fatec.sp.edu.br', max_length=100),
        ),
    ]
