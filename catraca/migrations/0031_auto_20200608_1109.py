# Generated by Django 3.0.6 on 2020-06-08 14:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catraca', '0030_auto_20200608_1105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pessoa',
            name='foto',
            field=models.ImageField(blank=True, null=True, upload_to='photo_profile'),
        ),
    ]
