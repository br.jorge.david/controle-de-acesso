from django.contrib import admin
from .models import Pessoa, Curso, Motivo
#admin.site.register(Pessoa)


@admin.register(Pessoa)
class PessoaAdmin(admin.ModelAdmin):
    list_display = ('nome', 'curso', 'ra', 'cpf', 'email')
    ordering = ('nome','email',)
    search_fields = ['nome']
    #prepopulated_fields = {"slug": ("curso",)}
    #admin.site.register(Pessoa, PessoaAdmin)

admin.site.register(Curso)
admin.site.register(Motivo)
