from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
import uuid
import os

#usuario
from django.forms import ClearableFileInput

#hash nome da imagem
def get_file_path(instance, filename):
    ext = filename.split(".")[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join("produtos", filename)

class Usuario(models.Model):
    STATUS_USER = (
        ("A", "Ativo"),
        ("I", "Inativo")
    )

    GRUPO_USER = (
        ("ADM", "Administrador"),
        ("INT", "Intermediario"),
        ("PAD", "Padrão")
    )

    nome = models.CharField(max_length=50, null=False, blank=False)
    user = models.CharField(max_length=30, null=False, blank=False)
    email = models.EmailField(max_length = 100, default = "")
    cpf = models.IntegerField()
    rg = models.IntegerField()
    grupo = models.CharField(max_length=3, choices=GRUPO_USER, blank=False, null=False)
    status = models.CharField(max_length=1, choices=STATUS_USER, blank=False, null=False)
    senha = models.CharField(max_length=12)


    def __str__(self):
        return self.user

#motivo
class Motivo(models.Model):
    descrmotivo = models.CharField(max_length=50, null=False, blank=False)
    obs = models.TextField()

    def __str__(self):
        return self.descrmotivo


#curso
class Curso(models.Model):
    descrcurso = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.descrcurso


#pessoa
class Pessoa(models.Model):
    ESCOLHA_SEXO = (
        ("F", "Feminino"),
        ("M", "Masculino"),
        ("N", "Não declarado")
    )
    TIPO_PESSOA = (
        ("A", "Aluno"),
        ("V", "Visitante"),
        ("F", "Funcionario")
    )

    STATUS_PESSOA = (
        ("A", "Ativo"),
        ("I", "Inativo")
    )
    nome = models.CharField(max_length=50, null=False, blank=False)
    foto = models.FileField(upload_to=get_file_path)
    curso = models.ForeignKey(Curso,  on_delete=models.CASCADE, default=None)
    sexo = models.CharField(max_length=1, choices=ESCOLHA_SEXO, blank=False, null=False)
    tipo = models.CharField(max_length=1, choices=TIPO_PESSOA, blank=False, null=False)
    ra = models.CharField(max_length=13, unique=True)
    cpf = models.CharField(max_length=11, unique=True)
    rg = models.CharField(max_length=14, unique=True)
    email = models.EmailField()
    telefone_fixo = models.CharField(max_length=10)
    telefone_movel = models.CharField(max_length=11)
    #foto = models.ImageField()
    status = models.CharField(max_length=1, choices=STATUS_PESSOA, blank=False, null=False)

    def __str__(self):
        return '%s %s %s %s %s %s %s %s %s %s %s %s' %  (self.nome,
                              self.curso,
                              self.sexo,
                              self.tipo,
                              self.ra,
                              self.cpf,
                              self.rg,
                              self.email,
                              self.telefone_fixo,
                              self.telefone_movel,
                              self.status,
                              self.id)



#catraca
class INOUT(models.Model):
    dtocorrencia =  models.DateTimeField(auto_now=True)
    codpessoa = models.ForeignKey(Pessoa, on_delete=models.CASCADE, default=None)
    codmotivo = models.ForeignKey(Motivo, on_delete=models.CASCADE, default=None)
    coduser = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
