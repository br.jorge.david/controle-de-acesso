class Motivo():
    def __init__(self, descrmotivo, obs):
        self.__descrmotivo = descrmotivo
        self.__obs = obs

    @property
    def descrmotivo(self):
        return self.__descrmotivo

    @descrmotivo.setter
    def descrmotivo(self, descrmotivo):
        self.__descrmotivo = descrmotivo

    @property
    def obs(self):
        return self.__obs

    @obs.setter
    def obs(self, obs):
        self.__obs = obs