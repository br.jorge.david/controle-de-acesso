class Pessoa():
    def __init__(self, nome, ra, cpf, rg,email, telefone_fixo, telefone_movel, status, tipo, sexo, curso, foto):
        self.__nome = nome
        self.__foto = foto
        self.__curso = curso
        self.__ra = ra
        self.__cpf = cpf
        self.__rg = rg
        self.__telefone_fixo = telefone_fixo
        self.__telefone_movel = telefone_movel
        self.__status = status
        self.__tipo = tipo
        self.__email = email
        self.__sexo = sexo

    @property
    def nome(self):
        return self.__nome

    @nome.setter
    def nome(self, nome):
        self.__nome = nome

    @property
    def foto(self):
        return self.__foto

    @foto.setter
    def foto(self, foto):
        self.__foto = foto

    @property
    def ra(self):
        return self.__ra

    @ra.setter
    def ra(self, ra):
        self.__ra = ra

    @property
    def cpf(self):
        return self.__cpf

    @cpf.setter
    def cpf(self, cpf):
        self.__cpf = cpf


    @property
    def rg(self):
        return self.__rg

    @rg.setter
    def rg(self, rg):
        self.__rg = rg

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, email):
        self.__email = email


    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, status):
        self.__status = status



    @property
    def tipo(self):
        return self.__tipo

    @tipo.setter
    def tipo(self, tipo):
        self.__tipo = tipo



    @property
    def telefone_fixo(self):
        return self.__telefone_fixo

    @telefone_fixo.setter
    def telefone_fixo(self, telefone_fixo):
        self.__telefone_fixo = telefone_fixo


    @property
    def telefone_movel(self):
        return self.__telefone_movel

    @telefone_movel.setter
    def telefone_movel(self, telefone_movel):
        self.__telefone_movel = telefone_movel


    @property
    def sexo(self):
        return self.__sexo

    @sexo.setter
    def sexo(self, sexo):
        self.__sexo = sexo


    @property
    def curso(self):
        return self.__curso

    @curso.setter
    def curso(self, curso):
        self.__curso = curso


