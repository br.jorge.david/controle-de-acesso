class Usuario():
    def __init__(self, nome, user, cpf, rg , grupo, status, senha, email):
        self.__nome = nome
        self.__user = user
        self.__cpf = cpf
        self.__rg = rg
        self.__grupo = grupo
        self.__status = status
        self.__senha = senha
        self.__email = email

    @property
    def nome(self):
        return self.__nome

    @nome.setter
    def nome(self, nome):
        self.__nome = nome

    @property
    def user(self):
        return self.__user

    @user.setter
    def user(self, user):
        self.__user = user

    @property
    def cpf(self):
        return self.__cpf

    @cpf.setter
    def cpf(self, cpf):
        self.__cpf = cpf

    @property
    def rg(self):
        return self.__rg

    @rg.setter
    def rg(self, rg):
        self.__rg = rg

    @property
    def grupo(self):
        return self.__grupo

    @grupo.setter
    def grupo(self, grupo):
        self.__grupo = grupo

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, status):
        self.__status = status

    @property
    def senha(self):
        return self.__senha

    @senha.setter
    def senha(self, senha):
        self.__senha = senha

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, email):
        self.__email = email

