from django import forms
from django.conf.global_settings import DATE_FORMAT

from .models import Pessoa, Curso, Motivo, Usuario
from django.forms.widgets import ClearableFileInput, DateInput

DATE_FORMAT = '%d/%m/%Y'

class CatracaForm(forms.Form):
    ra = forms.CharField(label='RA', max_length=30)
    motivo = forms.ModelChoiceField (queryset=Motivo.objects.all().order_by('descrmotivo'), widget=forms.Select, initial=5)

class PessoaPorStatusForm(forms.Form):
    STATUS_PESSOA = (
        ("A", "Ativo"),
        ("I", "Inativo"),
        ("T", "Todos")
    )
    status = forms.ChoiceField(choices=STATUS_PESSOA)

class AlunoPorCursoForm(forms.Form):
    curso = forms.ModelChoiceField(queryset=Curso.objects.all().order_by('descrcurso'), widget=forms.Select)


class ControleAcessoCpfForm(forms.Form):
    MODO_EXIBICAO = (
        ("CSV", "Gerar CSV"),
        ("Tela", "Exibir na Pagina")
    )
    cpf = forms.CharField(max_length=30)
    dataini = forms.CharField()
    datafin = forms.CharField()
    exibicao = forms.ChoiceField(choices=MODO_EXIBICAO)

class ControleAcessoRaForm(forms.Form):
    MODO_EXIBICAO = (
        ("CSV", "Gerar CSV"),
        ("Tela", "Exibir na Pagina")
    )
    ra = forms.CharField(max_length=30)
    dataini = forms.CharField()
    datafin = forms.CharField()
    exibicao = forms.ChoiceField(choices=MODO_EXIBICAO)

class ControleAcessoNomeForm(forms.Form):
    MODO_EXIBICAO = (
        ("CSV", "Gerar CSV"),
        ("Tela", "Exibir na Pagina")
    )
    nome = forms.CharField(max_length=30)
    dataini = forms.CharField()
    datafin = forms.CharField()
    exibicao = forms.ChoiceField(choices=MODO_EXIBICAO)

class ControleAcessoGeralForm(forms.Form):
    MODO_EXIBICAO = (
        ("CSV", "Gerar CSV"),
        ("Tela", "Exibir na Pagina")
    )
    dataini = forms.CharField()
    datafin = forms.CharField()
    exibicao = forms.ChoiceField(choices=MODO_EXIBICAO)

class ImportCsvPessoasForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )

class PesquisaPessoasForm(forms.Form):
    nome = forms.CharField(max_length=30, required=False)

class UsuarioForm(forms.ModelForm):
    senha = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Usuario
        fields = ['nome', 'user', 'email', 'cpf', 'rg', 'grupo', 'status', 'senha']


class UsuarioEditForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['nome', 'user', 'email', 'cpf', 'rg', 'grupo', 'status']

class PessoaForm(forms.ModelForm):
    foto = forms.ImageField(widget=ClearableFileInput)
    class Meta:
        model = Pessoa
        #fields = ['nome', 'ra', 'sexo', 'tipo', 'cpf', 'rg', 'email', 'telefone_fixo', 'telefone_movel', 'status', 'curso', 'foto']
        fields = '__all__'
        widgets = {'nome': forms.TextInput(attrs={'class':'form-control','maxlength': 255,'placeholder': 'Digite o nome'}),
                   'ra': forms.TextInput(attrs={'class': 'form-control', 'maxlength': 255, 'placeholder': 'Digite o ra'}),
                   'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digite o email'}),
                   'cpf': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digite o CPF'}),
                   'rg': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digite o RG'}),
                   'telefone_fixo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digite o Telefone Fixo'}),
                   'telefone_movel': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Digite o Telefone Celular'}),
        }


class MotivoForm(forms.ModelForm):
    class Meta:
        model = Motivo
        fields = ['descrmotivo', 'obs']

class CursoForm(forms.ModelForm):
    class Meta:
        model = Curso
        fields = ['descrcurso']